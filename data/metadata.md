# Metadaten:

## Datum:
- Alle Protokolle heruntergeladen am: 14.10.2018
- Protokolle der Wahlperioden 01. bis 17. von Seiten der Bundesregierung zuletzt geändert am: 09.07.2015
- Protokolle der 18. Wahlperiode von Seiten der Bundesregierung zuletzt geändert am: 09.01.2018
- Protokolle 15., 16. und 17. sind fehlerhaft. Wurden erstmal ausgegliedert.

## Herkunft.
- Quelle: https://www.bundestag.de/service/opendata

## Anzahl:

### Einzelne Wahlperioden:
01. Wahlperiode: 282
02. Wahlperiode: 227
04. Wahlperiode: 168
05. Wahlperiode: 198
03. Wahlperiode: 247
06. Wahlperiode: 199
08. Wahlperiode: 259
07. Wahlperiode: 230
09. Wahlperiode: 142
10. Wahlperiode: 256
11. Wahlperiode: 236
12. Wahlperiode: 243
13. Wahlperiode: 248
14. Wahlperiode: 253
15. Wahlperiode: 187
16. Wahlperiode: 233
17. Wahlperiode: 253
18. Wahlperiode: 245

### Grundgesamtheit:
**01. bis 18.** Wahlperiode: 4106
**01.bis einschließlich 14. und 18.** Wahlperiode: 3433

## Theoretische Stichprobengröße:
Die Stichprobe kann mit einer klastischen Standardformel berechnet werden.
Mit folgenden Input Werten:
- Populationsgröße/Grundgesamtheit: 4106
- Vertrauensintervall/Sicherheit/Irrtumswahrscheinlichkeit(dann 5%): 95%
- Fehlerspanne: 5%
- Population Proportion/ Antwortsverteilung: 50%
- Ergebnis: Stichprobengröße: 352
- Mit einer Fehlerspanne von 5,45% beträgt die Stichprobengröße 300 Ist denke ich akzeptabel.
- Neue Stichprobengröße beträgt um die 264 Protokolle
