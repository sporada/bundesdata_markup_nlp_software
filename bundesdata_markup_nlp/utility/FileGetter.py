#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import fnmatch

"""
This class is for getting filepaths of all files in a given directory. Also
gets files in subdirectories.
"""


class FileGetter(object):
    """
    Class for getting file paths of given path wich will be opend and/or
    further processed later on.
    """

    def __init__(self, path, file_type):
        super(FileGetter, self).__init__()
        self.path = path
        self.file_type = file_type

    def get_files(self):
        """
        Creates file list with full paths of all files in the given
        directory and its sub directories and returns it.
        """
        list_of_files = []
        for path, subdirs, files in os.walk(self.path):
            for name in files:
                if fnmatch.fnmatch(name, self.file_type):
                    list_of_files.append(os.path.join(path, name))
        self.list_of_files = list_of_files
        return list_of_files
