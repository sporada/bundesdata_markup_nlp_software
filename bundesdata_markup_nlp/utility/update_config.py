#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser


def update_config(file_name, section, key, value):
    """
    This script updates a config file identified by file_name. Updates the data
    of one key value pair in a specific section.
    """
    config = configparser.ConfigParser()
    config.read(file_name)
    file = open(file_name, "w")
    config.set(section, key, value)
    config.write(file)
    file.close()


if __name__ == '__main__':
    update_config()
