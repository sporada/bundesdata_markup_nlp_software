import os

"""
Helper script to move n_gram csvs to seperate folders. Just copy this into the
folder containing the n-grams and execute it. Change n to number of N in N-grams.
"""
current_path = os.getcwd()
files = []
n = 5
for file in os.listdir(current_path):
    if file.endswith(".csv"):
        files.append(file)
files = sorted(files)

dir_list = ["1_grams", "2_grams", "3_grams", "4_grams", "5_grams"][:n]
for dir in dir_list:
    os.system("mkdir {}".format(dir))

for step, dir in zip(range(0, n), dir_list):
    for file in files[step::n]:
        print(file)
        os.system("mv {} {}".format(file, dir))
