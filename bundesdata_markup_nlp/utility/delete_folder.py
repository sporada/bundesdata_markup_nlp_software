#!/usr/bin/env python
# -*- coding: utf-8 -*-

import shutil


def delete_folder(folder_path):
    """
    Deletes folder idetified by input folder path string.
    """
    shutil.rmtree(folder_path)


if __name__ == '__main__':
    delete_folder()
