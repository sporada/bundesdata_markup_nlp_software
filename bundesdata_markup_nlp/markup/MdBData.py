#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utility.XMLProtocol import XMLProtocol
import logging


class MdBData(XMLProtocol):
    """Class to handel operations on the Stammdatenbank."""

    def __init__(self):
        super(XMLProtocol, self).__init__()
        self.logger = logging.getLogger(__name__)

    def get_set(self, element_path, element_tree):
        """
        Creates Sets from input path on element_tree.
        """
        tmp_list = [element.text for element in
                    element_tree.iterfind(element_path) if element is not None]
        set_of_elements = set(tmp_list)
        return set_of_elements
